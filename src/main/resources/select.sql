# https://zhuanlan.zhihu.com/p/43289968 全部的练习题
# 查询课程编号为“01”的课程比“02”的课程成绩高的所有学生的学号
SELECT st.*, a.s_score, b.s_score
from Student as st
    inner join (select s_id, s_score from Score where c_id = '01') as a on st.s_id = a.s_id
    inner join (select s_id, s_score from Score where c_id = '02') as b on st.s_id = b.s_id
where a.s_score > b.s_score;

# 查询平均成绩大于60分的学生的学号和平均成绩
select st.s_name, sc.s_id, avg(sc.s_score) "平均成绩"
from Score sc
         inner join Student st on sc.s_id = st.s_id
group by sc.s_id having avg(sc.s_score) > 60;

# 查询各科成绩最高分、最低分和平均分：以如下形式显示：课程ID，课程name，最高分，最低分，平均分，及格率，中等率，优良率，优秀率
select c_id, max(s_score) max, min(s_score) min, avg(s_score) avg,
       avg(case when s_score >= 60 then 1.0 else 0.0 end ) ">=60及格率"
from Score group by c_id;

# 使用分段[100-85],[85-70],[70-60],[<60]来统计各科成绩，分别统计各分数段人数：课程ID和课程名称

# 第二高的薪水
select distinct salary from Employee order by Salary DESC limit 1,1;

select ifnull((select distinct salary from Employee order by Salary DESC limit 1,1),null) as SecondHighestSalary;

select FirstName, LastName, City, State from Person left join Address on Person.PersonId = Address.PersonId;
