package com.chao.base.designpattern.factorypattern;

/**
 * 创建一个餐馆的接口,因为这里只要有做菜就行，所以写一个cook的方法
 */
public interface Resaurant {
    void cook();
}
