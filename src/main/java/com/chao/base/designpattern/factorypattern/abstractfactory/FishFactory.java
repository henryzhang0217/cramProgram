package com.chao.base.designpattern.factorypattern.abstractfactory;

import com.chao.base.designpattern.factorypattern.Fish;
import com.chao.base.designpattern.factorypattern.Resaurant;

/**
 * 创建两个具体需要的产品实现类去继承上面这个抽象类
 */
public class FishFactory extends CookFactory {
    @Override
    public Resaurant createRestaurant() {
        return new Fish();
    }
}
