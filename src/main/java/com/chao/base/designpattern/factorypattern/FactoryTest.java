package com.chao.base.designpattern.factorypattern;

/**
 * 测试类
 */
public class FactoryTest {
    public static void main(String[] args) {
        Resaurant resaurant = Menu.getMean(Menu.MEAN_MEET);
        resaurant.cook();
    }
}
