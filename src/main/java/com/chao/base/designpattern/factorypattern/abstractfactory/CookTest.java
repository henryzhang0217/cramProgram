package com.chao.base.designpattern.factorypattern.abstractfactory;

import com.chao.base.designpattern.factorypattern.Resaurant;

/**
 * 抽象工厂测试类
 */
public class CookTest {
    public static void main(String[] args) {
        Resaurant duck = new DuckFactory().createRestaurant();
        duck.cook();
        Resaurant fish = new FishFactory().createRestaurant();
        fish.cook();
    }
}
