package com.chao.base.designpattern.factorypattern;

/**
 * 写三个实现类，分别是做回锅肉的，做鱼的，做烤鸭的，用这三个实现类去实现餐馆的接口
 */
public class Fish implements Resaurant {
    @Override
    public void cook() {
        System.out.println("来一份红烧鱼");
    }
}
