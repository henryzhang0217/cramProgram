package com.chao.base.designpattern.factorypattern.abstractfactory;

import com.chao.base.designpattern.factorypattern.Duck;
import com.chao.base.designpattern.factorypattern.Resaurant;

/**
 * 创建两个具体需要的产品实现类去继承上面这个抽象类
 */
public class DuckFactory extends CookFactory {
    @Override
    public Resaurant createRestaurant() {
        return new Duck();
    }
}
