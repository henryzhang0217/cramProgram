package com.chao.base.designpattern.factorypattern.abstractfactory;

import com.chao.base.designpattern.factorypattern.Resaurant;

/**
 * 创建一个抽象工厂类
 */
public abstract class CookFactory {
    public abstract Resaurant createRestaurant();
}
