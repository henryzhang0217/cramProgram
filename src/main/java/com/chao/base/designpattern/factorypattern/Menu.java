package com.chao.base.designpattern.factorypattern;

/**
 * 现在餐馆已经具备了做回锅肉，做鱼，做烤鸭的功能，但是客人来了并不知道餐馆有这些菜，这时候就需要我们来给餐馆做一个菜单，客人来了就可以根据菜单点餐，这样就省去了很多的麻烦对不对
 */
public class Menu {

    public static final int MEAN_MEET = 1;
    public static final int MEAN_FISH = 2;
    public static final int MEAN_DUCK = 3;

    public static Resaurant getMean(int meantype){
        switch (meantype){
            case MEAN_MEET:
                return new Meet();
            case MEAN_FISH:
                return new Fish();
            case MEAN_DUCK:
                return new Duck();
            default:
                return null;
        }
    }
}
