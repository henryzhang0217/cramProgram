package com.chao.base.designpattern.singLetonpattern;

/**
 * 双检锁/双重校验锁（DCL，即 double-checked locking）
 */
public class SingletonLock {

    private volatile static SingletonLock instance;
    private SingletonLock(){};
    public static SingletonLock getInstance(){
        if (instance == null){
            synchronized (SingletonLock.class){
                if (instance == null)
                    instance = new SingletonLock();
            }
        }
        return instance;
    }
}
