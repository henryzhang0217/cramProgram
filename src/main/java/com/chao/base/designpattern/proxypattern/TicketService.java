package com.chao.base.designpattern.proxypattern;

/**
 * 建一个售票服务的接口，它有售票咨询和退票的业务可以供客户选择
 */
public interface TicketService {
    //售票
    void SellTicket();
    //咨询
    void Consultation();
    //退票
    void ReturnTicket();
}
