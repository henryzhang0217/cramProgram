package com.chao.base.designpattern.proxypattern;

/**
 * 创建一个售票服务接口实现类，就好比是车站
 */
public class Station implements TicketService{
    @Override
    public void SellTicket() {
        System.out.println("售票");
    }

    @Override
    public void Consultation() {
        System.out.println("咨询");
    }

    @Override
    public void ReturnTicket() {
        System.out.println("退票");
    }
}
