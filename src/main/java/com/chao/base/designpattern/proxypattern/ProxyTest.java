package com.chao.base.designpattern.proxypattern;

/**
 * 创建购买车票的角色,去代理点完成购买车票的需求
 */
public class ProxyTest {
    public static void main(String[] args) {
        Station station = new Station();
        StationProxy proxy = new StationProxy(station);
        proxy.SellTicket();
    }
}
