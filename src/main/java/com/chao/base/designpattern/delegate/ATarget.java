package com.chao.base.designpattern.delegate;

/**
 * A员工做具体的事情
 */
public class ATarget implements Target {
    @Override
    public void doSomething(String commond) {
        System.out.println("A员工做具体的事情"+ commond + "");
    }
}
