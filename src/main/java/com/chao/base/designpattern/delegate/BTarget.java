package com.chao.base.designpattern.delegate;

public class BTarget implements Target {
    @Override
    public void doSomething(String commond) {
        System.out.println("B员工做具体的事情"+ commond + "");
    }
}
