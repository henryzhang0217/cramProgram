package com.chao.base.designpattern.delegate;

/**
 * Boss给leader直接下达命令
 */
public class DelegateTest {
    public static void main(String[] args) {
        new Leader().doSomething("打印文件");
    }
}
