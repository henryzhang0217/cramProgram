package com.chao.base.designpattern.delegate;

/**
 * 创建一个员工干活的接口Target
 */
public interface Target {

    public void doSomething(String commond);
}
