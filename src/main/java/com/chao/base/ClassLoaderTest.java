package com.chao.base;

public class ClassLoaderTest {
    public static void main(String[] args) throws ClassNotFoundException{
        ClassLoader loader = ClassLoaderTest.class.getClassLoader();

        //app
        System.out.println(loader);
        //ext
        System.out.println(loader.getParent());
        //bootstarp
        System.out.println(loader.getParent().getParent());
    }
}
