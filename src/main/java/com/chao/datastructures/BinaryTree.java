package com.chao.datastructures;

import java.util.LinkedList;
import java.util.Queue;

/**
 * 二叉树 https://zhuanlan.zhihu.com/p/81290282
 */
public class BinaryTree {

    //创建根节点
    Node root;

    /**
     * 添加节点
     * @param value
     */
    public void add(int value){
        root = addRecursive(root, value);
    }

    /**
     * 是否存在节点
     * @param value
     * @return
     */
    public boolean contains(int value){
        return containsRecursive(root, value);
    }

    /**
     * 循环添加 并且左子树小于右子树
     * @param curr
     * @param value
     * @return
     */
    public Node addRecursive(Node curr, int value){
        if (curr == null){
            return new Node(value);
        }
        if (value < curr.value){
            curr.left = addRecursive(curr.left, value);
        } else if (value > curr.value){
            curr.right = addRecursive(curr.right, value);
        } else {
            return curr;
        }
        return curr;
    }


    /**
     *是否存在节点
     * @param curr
     * @param value
     * @return
     */
    public boolean containsRecursive(Node curr, int value){
        if (curr == null){
            return false;
        }
        if (value == curr.value){
            return true;
        }
        return  value < curr.value
                ? containsRecursive(curr.left, value)
                : containsRecursive(curr.right, value);
    }

    /**
     * pre-order：首先访问根节点，然后是左子树，最后是右子树：
     */
    public void traversePreOrder(Node node){
        if (node != null) {
            System.out.print(" " + node.value);
            traversePreOrder(node.left);
            traversePreOrder(node.right);
        }
    }

    /**
     * in-order：首先访问左子树，然后访问根节点，最后访问右子树：
     */
    public void traverseInOrder(Node node){
        if (node != null) {
            traverseInOrder(node.left);
            System.out.print(" " + node.value);
            traverseInOrder(node.right);
        }
    }

    /**
     * post-order：访问左子树，右子树，最后访问根节点：
     */
    public void traversePostOrder(Node node) {
        if (node != null) {
            traversePostOrder(node.left);
            traversePostOrder(node.right);
            System.out.print(" " + node.value);
        }
    }

    /**
     * 广度优先搜索 在展示进入下一级别之前访问级别的所有节点
     */
    public void traverseLevelOrder(){
        if (root == null)
            return;

        Queue<Node> nodes = new LinkedList<>();
        nodes.add(root);

        while (!nodes.isEmpty()){
            Node node = nodes.remove();
            System.out.print(" " + node.value);
            if (node.left != null)
                nodes.add(node.left);
            if (node.right != null)
                nodes.add(node.right);
        }
    }

    /**
     * 测试方法
     */
    public BinaryTree createBinaryTree(){
        BinaryTree bt = new BinaryTree();
        bt.add(6);
        bt.add(4);
        bt.add(8);
        bt.add(3);
        bt.add(5);
        bt.add(7);
        bt.add(9);
        //System.out.println(bt);
        return bt;
    }

    /**
     * 主方法
     * @param args
     */
    public static void main(String[] args) {
        BinaryTree bt = new BinaryTree();
        BinaryTree btC = bt.createBinaryTree();

    }
}
