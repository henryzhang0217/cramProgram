package com.chao.datastructures;

import lombok.Data;

/**
 * 单链表
 * https://labuladong.gitbook.io/algo/shu-ju-jie-gou-xi-lie/di-gui-fan-zhuan-lian-biao-de-yi-bu-fen
 */
@Data
public class ListNode {
    public int data;
    public ListNode next;

    public ListNode(int data){
        this.data = data;
        next = null;
    }

    public void connect(ListNode head, ListNode next){
        if (head != null)
            head.next = next;
    }

    /**
     * 递归-反转链表
     * @param head
     * @return
     */
    public ListNode reverseRecursion(ListNode head){
        if (head.next == null)
            return head;
        ListNode last = reverseRecursion(head.next);
        head.next.next = head;
        head.next = null;
        return last;
    }

    /**
     * 递归-反转链表的前N个节点
     * @param head
     * @param n
     * @return
     */
    ListNode successor = null;
    public ListNode reverseRecursionN(ListNode head, int n){
        //bad case
        if (n == 1){
            successor = head.next;
            return head;
        }
        // 以head.next 为起点，需要反转前 n - 1 个节点
        ListNode last = reverseRecursionN(head.next, n-1);
        head.next.next = head;
        head.next = successor;
        return last;
    }

    /**
     * 递归-反转链表M-N区间
     * @param head
     * @param m
     * @param n
     * @return
     */
    public ListNode reverseRecursionBetween(ListNode head, int m, int n){
        if (m == 1)
            return reverseRecursionN(head, n);
        //前进到反转的起点触发 base case
        head.next = reverseRecursionBetween(head.next, m -1, n - 1);
        return head;
    }

    /**
     * 迭代-反转链表
     * @param head
     * @return
     */
    public ListNode reverseIterate(ListNode head){
        ListNode cur = head, pre = null, next = null;
        while (null != head){
            //保留当前节点的下一个节点
            next = head.next;
            //重置当前节点的下一个节点为上一个节点
            cur.next = pre;
            //下次遍历需要用到的上一个节点
            pre = cur;
            //下次遍历需要用到的当前节点
            cur = next;
        }
        return pre;
    }
}
