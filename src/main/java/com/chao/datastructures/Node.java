package com.chao.datastructures;

/**
 * 二叉树的节点
 */
public class Node {
    int value;
    Node left;
    Node right;

    /**
     * 构造方法
     * @param value 节点
     */
    Node(int value){
        this.value = value;
        left = null;
        right = null;
    }
}
