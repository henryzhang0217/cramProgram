package com.chao.spring.interfaceimpl;

import com.chao.spring.interfaces.GreetingService;
import org.springframework.stereotype.Service;

@Service
public class GreetingServiceImpl implements GreetingService {

    @Override
    public String sayHello(String name) {
        return "hello" + name;
    }
}
