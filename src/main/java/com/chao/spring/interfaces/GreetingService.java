package com.chao.spring.interfaces;

public interface GreetingService {

    String sayHello(String name);
}
