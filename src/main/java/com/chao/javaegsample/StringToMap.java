package com.chao.javaegsample;


import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class StringToMap {

    public static void main(String[] args) {

        String sb = "shopCode@GRID_YM_CSNBZYDELIVERY01" +
                "&asdpShop@RT_NB_Railway" +
                "&rs@2020071613001700" +
                "&routeArea@64387" +
                "&logisticsLines@11603" +
                "&warehouseCode@RT_NB_Railway" +
                "&fulfillTag@HALF_DAY" +
                "&asdpChannelShop@324286002" +
                "&asdpMcWarehouse@TCLP_V_RT_NB_Railway" +
                "&isMLT@false" +
                "&isCalculatePackageTimeSliceChosenEarliestSku@false" +
                "&fulfillTagDetail@HALF_DAY" +
                "&food_process_time@0" +
                "&asdpMerchantCode@CSDARUNFA";

        Map<String, String> map = Pattern.compile("\\s*&\\s*")
                .splitAsStream(sb.trim())
                .map(s -> s.split("@", 2))
                .collect(Collectors.toMap(a -> a[0], a -> a.length > 1 ? a[1] : "notExit"));

        System.out.println(map);
        //map.containsKey("logisticsLines");
        //遍历map获取value为notExit的key
    }
}
