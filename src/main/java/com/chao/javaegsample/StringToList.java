package com.chao.javaegsample;

import java.util.Arrays;
import java.util.regex.Pattern;

public class StringToList {
    public static void main(String[] args) {

        String promRO = "1123454, 1123432, 1125654, 1134231";

        long[] promList = Pattern.compile("\\s*,\\s*")
                .splitAsStream(promRO.trim())
                .mapToLong(Long :: parseLong)
                .toArray();

        System.out.println(Arrays.toString(promList));
    }
}
