package com.chao.algorithm;

/**
 * 二分查找 https://labuladong.gitbook.io/algo/suan-fa-si-wei-xi-lie/er-fen-cha-zhao-xiang-jie
 * 小技巧：不要出现 else，而是把所有情况用 else if 写清楚，这样可以清楚地展现所有细节
 */
public class BinarySearch {

    /**
     * 基本二分搜索 双闭区间 [left, right]
     * @param nums 给定的数组
     * @param target 目标值
     * @return
     */
    public int binarySearch(int[] nums, int target){
        int left = 0;
        int right = nums.length - 1;
        //搜索区间为 [left, right]
        while (left <= right){
            int mid = left + (left + right)/2;
            if (nums[mid] == target){
                return mid;
            } else if (nums[mid] < target){
                left = mid + 1;
            }else if (nums[mid] > target){
                right = mid - 1;
            }
        }
        return -1;
    }

    /**
     * 左边界的二分查找 左闭右开 [left, right)
     * @param nums 给定的数组 nums = [1,2,2,2,3]
     * @param target 目标值
     * @return
     */
    public int binarySearchleftbound(int[] nums, int target){


        return -1;
    }
}
