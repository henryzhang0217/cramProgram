package com.chao.algorithm.interview;

import java.util.ArrayList;
import java.util.List;

/**
 * 去除驼峰字符串
 */
public class HumpSubStr {

    public String humpSubStr(String s) {
        List<Character> chars = new ArrayList<Character>();
        for (int i = 0; i < s.length(); i++){
            chars.add(s.charAt(i));
        }
        int originalLen = chars.size();
        int i = 0;
        while (chars.size() > 2){
            if (chars.get(i)==chars.get(i+2)&&chars.get(i)!=chars.get(i+1)){
                chars.remove(i);
                chars.remove(i);
                chars.remove(i);
            }
            i++;
            int subLen = chars.size();
            if (subLen != originalLen){
                originalLen = chars.size();
                i = 0;
            }
        }
        return chars.toString();
    }

    public static void main(String[] args) {
        HumpSubStr humpSubStr = new HumpSubStr();
        String target = "abcbdaef";
        String result = humpSubStr.humpSubStr(target);
        System.out.println(result);
    }
}
