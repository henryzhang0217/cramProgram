package com.chao.algorithm.interview;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Java实现抢红包算法，附完整代码（公平版和手速版）
 * https://zhuanlan.zhihu.com/p/257116740
 */
public class RedPackage {

    /**
     * 生成min到max范围的浮点数
     */
    public double nextDouble(double min, double max){
        return min + ((max - min) * new Random().nextDouble());
    }

    //格式化
    public String format(double value){
        return new DecimalFormat("0.00").format(value);
    }

    //二倍均值法
    public List<Double> doubleMeanMethod(Double money, int number){
        List<Double> result = new ArrayList<>();
        if (money < 0 && number < 1){
            return null;
        }
        double amount, sum=0;
        int remainingNumber = number;
        int i = 1;
        while (remainingNumber > 1){
            amount = nextDouble(0.01, 2*(money / remainingNumber));
            sum += amount;
            System.out.println("第" + i + "个人领取的红包金额为：" + format(amount));
            money -= amount;
            remainingNumber --;
            result.add(amount);
            i ++;
        }
        result.add(money);
        System.out.println("第"+i+"个人领取的红包金额为："+format(money));
        sum += money;
        System.out.println("验证发出的红包总金额为："+format(sum));
        return result;
    }

    public static void main(String[] args) {
        RedPackage redPackage = new RedPackage();
        double money = 100;
        int number = 10;
        redPackage.doubleMeanMethod(money, number);
        //double min = 4;
        //double max = 10;
        //System.out.println(redPackage.nextDouble(min, max));
    }
}
