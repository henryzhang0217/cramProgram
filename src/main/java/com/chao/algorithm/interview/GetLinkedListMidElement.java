package com.chao.algorithm.interview;


import com.chao.datastructures.ListNode;

/**
 * 一次遍历获得单链表的中间元素
 * 快慢指针
 */
public class GetLinkedListMidElement {


    private ListNode getLinkedListMidElement(ListNode root){
        ListNode slow = root;
        ListNode fast = root.next;

        while (fast != null && fast.next != null){
            slow = slow.next;
            fast = fast.next.next;
        }
        return fast == null ? slow : slow.next;
    }

}
