package com.chao.algorithm.interview;

/**
 * 已知random7求random10 https://blog.csdn.net/zhou_yujia/article/details/84034127
 * random7()返回的1,2,3,4,5,6,7
 * 经过加减乘除这类的运算至少应该是扩充到比10大的数据范围，才能进行取模运算
 * 那么应该想到7*(random7()-1)+random7() 可以扩充到1-49而且是等概率的
 * 比49小的最大的能被10整除的是40，那么大于40的就舍弃
 * https://blog.csdn.net/majianfei1023/article/details/7249918
 */
public class Random7ToRandom10 {

    public int random7(){
        return (int) ((Math.random() * 7) + 1);
    }

    public int random10(){
        int x = 49;
        while (x > 40){
            x = random7() * (random7() - 1) + random7();
        }
        return x%10 + 1;
    }

    public static void main(String[] args) {
        Random7ToRandom10 r = new Random7ToRandom10();
        for (int i = 0; i < 50; i++){
            System.out.println(r.random10());
        }
    }
}
