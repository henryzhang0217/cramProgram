package com.chao.algorithm.dynamicprogramming;

/**
 * 最大子数组 https://labuladong.gitbook.io/algo/dong-tai-gui-hua-xi-lie/zui-da-zi-shu-zu
 */
public class MaximumSubarray {

    int maxSubArray(int[] nums){
        int length = nums.length;
        if (length == 0)
            return 0;
        int dp_0 = nums[0];
        int dp_1 = 0;
        int res = dp_0;
        for (int i = 1; i < length; i++){
            dp_1 = Math.max(nums[i], nums[i] + dp_0);
            dp_0 = dp_1;
            res = Math.max(res, dp_1);
        }
        return res;
    }
}
