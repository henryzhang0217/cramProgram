package com.chao.algorithm.leetcode;

/**
 * 35. 搜索插入位置 https://leetcode-cn.com/problems/search-insert-position/
 */
public class SearchAdInsert {

    public int searchInsert(int[] nums, int target){
        int l = -1;
        int m = nums.length;
        while (l + 1 < m){
            int n = (l + m)/2;
            if (nums[n] >= target){
                m = n;
            } else {
                l = n;
            }
        }
        return m;
    }
}
