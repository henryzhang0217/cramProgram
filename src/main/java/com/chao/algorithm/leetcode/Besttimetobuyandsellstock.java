package com.chao.algorithm.leetcode;

/**
 * 121. 买卖股票的最佳时机
 */
public class Besttimetobuyandsellstock {

    public int Solution(int[] prices){
        int n = prices.length;
        int dp_i_0 = 0;
        int dp_i_1 = Integer.MIN_VALUE;
        for (int i = 0; i < n; i++){
            dp_i_0 = Math.max(dp_i_0, dp_i_1 + prices[i]);
            dp_i_1 = Math.max(dp_i_1, -prices[i]);
        }
        return dp_i_0;
    }

    public static void main(String[] args) {
        Besttimetobuyandsellstock stock = new Besttimetobuyandsellstock();
        int[] prices = new int[]{7,1,5,3,6,4,1};
        System.out.println(stock.Solution(prices));
    }

}
