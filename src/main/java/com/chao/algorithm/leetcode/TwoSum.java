package com.chao.algorithm.leetcode;

import java.util.Arrays;

/**
 * 1. 两数之和
 */
public class TwoSum {

    public int[] Solution(int[] nums, int target){
        for (int i = 0; i < nums.length; i ++){
            int result = target - nums[i];
            for (int j = i+1; j < nums.length; j ++){
                if (result == nums[j]){
                    return new int[]{i, j};
                }
            }
        }
        throw new IllegalArgumentException("No two sum solution");
    }

    public static void main(String[] args) {
        int[] nums = new int[]{1,2,3,4,5,6};
        int target = 11;
        TwoSum twoSum = new TwoSum();
        System.out.println(Arrays.toString(twoSum.Solution(nums, target)));
    }
}
