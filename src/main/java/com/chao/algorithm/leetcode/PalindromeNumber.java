package com.chao.algorithm.leetcode;

/**
 * 9. 回文数 https://leetcode-cn.com/problems/palindrome-number/
 * 解题视频：https://www.bilibili.com/video/BV17J411F7CA?from=search&seid=15721898692978700702
 */
public class PalindromeNumber {

    public boolean Solution(int n){
        String num = Integer.toString(n);
        int l = 0;
        int r = num.length() - 1;
        while (l < r){
            if (num.charAt(l) != num.charAt(r))
                return false;
            l++;
            r--;
        }
        return true;
    }

    public static void main(String[] args) {
        PalindromeNumber palindromeNumber = new PalindromeNumber();
        System.out.println(palindromeNumber.Solution(12231));
    }
}
