package com.chao.algorithm.leetcode;

/**
 * 二叉树的最大深度
 */
public class maxDepthOfTreeNode {

    class TreeNode{
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode(int x){val = x;}
    }

    public int Solution(TreeNode root){
        if (root == null)
            return 0;
        int left = Solution(root.left);
        int right = Solution(root.right);
        return Math.max(left, right) + 1;
    }
}
