package com.chao.algorithm.leetcode;

/**
 * 53. 最大子序和
 * 动态规划：若前一个元素大于0，则加到当前元素上；得到新的数组，取新数组中最大值即可；
 * https://leetcode-cn.com/problems/maximum-subarray/solution/zui-da-zi-xu-he-by-leetcode-solution/ 看这里头部的视频即可
 */
public class MaximumSubarray {

    public int Solution(int[] nums){
        int ans = nums[0];
        int sum = 0;
        for (int x : nums){
            if (sum > 0){
                sum += x;
            } else {
                sum = x;
            }
            ans = Math.max(ans, sum);
        }
        return ans;
    }

    public static void main(String[] args) {
        int[] nums = new int[]{-2,1,-3,4,-1,2,1,-5,4};
        MaximumSubarray max = new MaximumSubarray();
        System.out.println(max.Solution(nums));
    }
}
