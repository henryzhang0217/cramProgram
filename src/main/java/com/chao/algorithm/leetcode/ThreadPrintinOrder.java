package com.chao.algorithm.leetcode;

import java.util.concurrent.atomic.AtomicInteger;

/**
 *  1114.线程按序打印 https://leetcode-cn.com/problems/print-in-order/
 */
public class ThreadPrintinOrder {
    private AtomicInteger firstJobDone = new AtomicInteger(0);
    private AtomicInteger secondJobDone = new AtomicInteger(0);

    public void first(Runnable printFirst) throws InterruptedException {

        // printFirst.run() outputs "first". Do not change or remove this line.
        printFirst.run();
        firstJobDone.incrementAndGet();
    }

    public void second(Runnable printSecond) throws InterruptedException {
        while (firstJobDone.get() != 1){
            //waiting
        }
        printSecond.run();
        secondJobDone.incrementAndGet();
        // printSecond.run() outputs "second". Do not change or remove this line.

    }

    public void third(Runnable printThird) throws InterruptedException {
        while (secondJobDone.get() != 1){
            //wating
        }
        // printThird.run() outputs "third". Do not change or remove this line.
        printThird.run();
    }
}
