package com.chao.algorithm.leetcode;

/**
 * 21. 合并两个有序链表
 * https://leetcode-cn.com/problems/merge-two-sorted-lists/solution/he-bing-liang-ge-you-xu-lian-biao-by-leetcode-solu/
 */
public class MergeTwoSortedLists {

    class ListNode{
        int value;
        ListNode next;

        ListNode(){}
        ListNode(int value){this.value = value;}
        ListNode(int value, ListNode next){this.value = value; this.next = next;}
    }

    public ListNode Solution(ListNode l1, ListNode l2){
        ListNode prehead = new ListNode(-1);
        ListNode pre = prehead;

        while (l1 != null && l2 != null){
            if (l1.value <= l2.value){
                pre.next = l1;
                l1 = l1.next;
            } else {
                pre.next = l2;
                l2 = l2.next;
            }
        }
        // 合并后 l1 和 l2 最多只有一个还未被合并完，我们直接将链表末尾指向未合并完的链表即可
        pre.next = l1 == null ? l2:l1;
        return prehead.next;
    }
}
