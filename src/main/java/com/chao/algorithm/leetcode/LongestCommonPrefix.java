package com.chao.algorithm.leetcode;

/**
 * 14. 最长公共前缀
 */
public class LongestCommonPrefix {

    public String longestCommonPrefix(String[] target){
        if (target.length == 0)
            return "";
        String prefix = target[0];
        for (int i = 0; i < target.length; i++){
            while (target[i].indexOf(prefix) != 0){
                prefix = prefix.substring(0, prefix.length() - 1);
                if (prefix.isEmpty())
                    return "";
            }
        }
        return prefix;
    }

    public static void main(String[] args) {
        String[] target = new String[]{"abc", "abcd", "abe", "abce"};
        LongestCommonPrefix prefix = new LongestCommonPrefix();
        System.out.println(prefix.longestCommonPrefix(target));
    }

}
