package com.chao.algorithm.leetcode;

/**
 * 198. 打家劫舍 https://leetcode-cn.com/problems/house-robber/
 * https://www.bilibili.com/video/BV1BE41187dF
 */
public class HouseRobber {

    public int houseRobber(int[] nums){
        int n = nums.length;
        if (n == 0) return 0;
        int[][] dp = new int[n][2];
        dp[0][0] = 0;
        dp[0][1] = nums[0];
        for (int i = 1; i < n; i++){
            dp[i][0] = Math.max(dp[i-1][0],dp[i-1][1]);
            dp[i][1] = dp[i-1][0] + nums[i];
        }
        return Math.max(dp[n-1][0], dp[n-1][1]);
    }
}
