package com.chao.algorithm.leetcode;

/**
 * 206. 反转链表
 * 视频讲解：https://www.bilibili.com/video/BV12V411k7tK?from=search&seid=17573194299764170251
 */
public class ReverseLinkedList {

    class Node{
        int data;
        Node next;

        public Node(int data){
            this.data = data;
        }

        public void connect(Node head, Node next){
            if (head != null)
            head.next = next;
        }
    }

    public Node Solution(Node head){
        Node pre = null;
        Node curr = head;

        while (curr != null){
            Node next = curr.next;
            curr.next = pre;
            pre = curr;
            curr = next;
        }
        return pre;
    }

}
