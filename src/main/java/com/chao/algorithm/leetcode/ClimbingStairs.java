package com.chao.algorithm.leetcode;

/**
 * 70. 爬楼梯 https://leetcode-cn.com/problems/climbing-stairs/
 */
public class ClimbingStairs {

    public int Solution(int n){
        int[] dp = new int[n + 2];
        dp[1] = 1;
        dp[2] = 2;
        for (int i = 3; i <= n; i++){
            dp[i] = dp[i-1] + dp[i-2];
        }
        return dp[n];
    }
}
