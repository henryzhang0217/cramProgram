package com.chao.algorithm.leetcode;

import java.util.LinkedList;
import java.util.Stack;

/**
 * 20. 有效的括号
 * https://leetcode-cn.com/problems/valid-parentheses/solution/you-xiao-de-gua-hao-by-leetcode-solution/
 */
public class ValidParenthesis {

    public boolean Solution(String s){
        LinkedList<Character> stack = new LinkedList();
        for (char c : s.toCharArray()){
            if (c == '[')
                stack.push(']');
            else if (c == '(')
                stack.push(')');
            else if (c == '{')
                stack.push('}');
            else if (stack.isEmpty() || c != stack.pop())
                return false;
        }
        return stack.isEmpty();
    }

    public boolean Solution1(String s){
        Stack<Character> stack = new Stack();
        for (int i = 0; i < s.length(); i++){
            char c = s.charAt(i);
            if (c == '(' || c == '[' || c == '{'){
                stack.push(c);
            } else {
                if (stack.isEmpty())
                    return false;
                char l = stack.pop();
                if (c == ')'){
                    if (l != '(')
                        return false;
                } else if (c == ']'){
                    if (l != '[')
                        return false;
                } else if ((c == '}')){
                    if (l != '{')
                        return false;
                }

            }
        }
        return stack.isEmpty();
    }

    public static void main(String[] args) {
        ValidParenthesis validParenthesis = new ValidParenthesis();
        String s = "()[]{}";
        System.out.println(validParenthesis.Solution1(s));
    }
}
