package com.chao.algorithm.leetcode;

/**
 * 88. 合并两个有序数组 https://leetcode-cn.com/problems/merge-sorted-array/
 * https://www.bilibili.com/video/BV1eE411y7WC
 */
public class mergesortedarray {

    public void Solution(int[] nums1, int m, int[] nums2, int n){
        int i1 = m - 1;
        int i2 = n - 1;
        int i = m + n - 1;
        while (i1 >=0 && i2 >= 0){
            if (nums1[i1] > nums2[i2] ){
                nums1[i] = nums1[i1];
                i1--;
                i--;
            } else {
                nums1[i] = nums2[i2];
                i2 --;
                i --;
            }
        }
        while (i2 >= 0){
            nums1[i] = nums2[i2];
            i--;
            i2--;
        }
    }

}
