package com.chao.algorithm.leetcode;

/**
 * 101 镜像对称二叉树 https://leetcode-cn.com/problems/symmetric-tree/
 * 视频 https://www.bilibili.com/video/BV1CJ411S7Zp
 */
public class SymmetricTree {

    class TreeNode{
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode(int n){
            val = n;
        }
    }

    public boolean isSymmetric(TreeNode root){
        if (root == null)
            return true;
        return isSymmetric(root.left, root.right);
    }

    public boolean isSymmetric(TreeNode left, TreeNode right){
        if (left == null && right == null)
            return true;
        if (left == null || right == null)
            return false;
        if (left.val != right.val)
            return false;
        return isSymmetric(left.right, right.left)&&isSymmetric(left.left, right.right);
    }
}
