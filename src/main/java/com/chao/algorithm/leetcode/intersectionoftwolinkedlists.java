package com.chao.algorithm.leetcode;

import com.chao.datastructures.ListNode;

/**
 * 160. 相交链表 https://leetcode-cn.com/problems/intersection-of-two-linked-lists/
 */
public class intersectionoftwolinkedlists {

    public ListNode Solution(ListNode headA, ListNode headB){
        ListNode pA = headA;
        ListNode pB = headB;
        while (pA != pB){
            pA = pA == null? headB:pA.next;
            pB = pB == null? headA:pB.next;
        }
        return pA;
    }
}
